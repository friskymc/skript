## Disease ##
This folder contains code in which new afflictions can occur from mob attacks or environmental factors.



- If a file has not been worked on you are free to edit it in any way you deem.
- If a file is in the process of being edited but you would like to work on it as well, please create a new branch and we can decide between both at a later date.
-If you have ideas that can be incorporated into this folders general idea, feel free to create another file.    

Please keep diseases separate file wise, but if you need to add a craftable cure, or a new NPC cure trade  etc, please put that code in the cure.sk file.


----------
*Any additional custom recipes should be also noted on the Custom Recipe.txt in the main folder of the repo. *                     